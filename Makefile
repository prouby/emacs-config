## Makefile --- Personal GNUEmacs configuration.  -*- lexical-binding: t; -*-

# Copyright (C) 2018-2021 Pierre-Antoine Rouby

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

EMACS = emacs --batch -nw -load point_emacs.el
EMACS_COMPILE = -f emacs-lisp-byte-compile
EMACS_DIR = ~/.emacs.d/

.PHONY: clean

all: compile

compile: point_emacs.elc

%.elc: %.el
	$(info Compiling    $<)
	@$(EMACS) $< $(EMACS_COMPILE)

install: compile
	$(info Install)
	@cp *.el  $(EMACS_DIR) -v
	@cp *.elc $(EMACS_DIR) -v

uninstall:
	$(info Uninstall)
	@rm -f $(EMACS_DIR)point_emacs.el  -v
	@rm -f $(EMACS_DIR)point_emacs.elc -v

clean:
	@rm -f *.elc

## Makefile ends here
