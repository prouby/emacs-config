;;; point_emacs.el --- Presonal GNUEmacs configuration.  -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2018-2019 Pierre-Antoine Rouby
;;
;; Author: Pierre-Antoine Rouby <contact@parouby.fr>
;; Keywords: lisp
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Configuration GNUEmacs.

;;; Code:

;;;
;;; Global variable
;;;

(defvar point-emacs-user-name       "Pierre-Antoine Rouby")
(defvar point-emacs-user-short-name "prouby")
(defvar point-emacs-user-email      "contact@parouby.fr")
(defvar point-emacs-user-pseudo     "prouby")

(defvar point-emacs-sources "https://framagit.org/prouby/emacs-config.git")

;;;
;;; Package config
;;;

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))

(package-initialize)

;;; Needed extention
(require 'guix)
(require 'helm)
(require 'magit)
(require 'emojify)
;;(require 'elmacro)
(require 'paredit)
(require 'company)
(require 'flycheck)
(require 'elisp-refs)
(require 'multi-term)
(require 'helm-flycheck)
(require 'proof-general)
(require 'doom-modeline)
(require 'cyberpunk-theme)
(require 'gitignore-templates)

;;; Additionnals mode
(require 'tuareg)                       ; Ocaml
(require 'hy-mode)
(require 'lua-mode)
(require 'iasm-mode)
(require 'nasm-mode)
(require 'nyan-mode)
(require 'rust-mode)
(require 'cmake-mode)
(require 'julia-mode)
(require 'racket-mode)
(require 'strace-mode)
(require 'simple-httpd)
(require 'haskell-mode)
(require 'markdown-mode)
(require 'gitlab-ci-mode)

;;;
;;; Display
;;;

;;; Disable menu and tool bar
(menu-bar-mode 0)
(tool-bar-mode 0)

(doom-modeline-mode)

;;; Full screen
(when (display-graphic-p)
  (add-to-list 'default-frame-alist
               '(fullscreen . maximized)))

(recentf-mode 1)                        ; Save recent file list
(display-time-mode t)                   ; Active time mode

;; (load-theme 'cyberpunk t)
(if (display-graphic-p)
    (load-theme 'deep-nebula t)
  (load-theme 'cyberpunk t))

;;;
;;; Various mode config
;;;

;;; Emacs startup hook (disable by default)
;; (add-hook 'emacs-startup-hook
;;           (lambda ()
;;             (with-current-buffer (window-buffer (split-window-right))
;;               (find-file "~/work/org/todo")
;;               (outline-show-all)
;;               (org-display-inline-images))))

;;; Delete trailing whitespace
(add-hook 'before-save-hook
          'delete-trailing-whitespace)

(add-hook 'after-init-hook 'nyan-mode)

;;; Company
(add-hook 'after-init-hook 'global-company-mode)
(setq company-idle-delay 0.2)
(setq company-minimum-prefix-length 3)
;;(setq company-selection-wrap-around t)

;;; Org mode
;; https://orgmode.org/worg/org-tutorials/org-latex-export.html
;;
(add-hook 'org-mode-hook 'column-number-mode)
(add-hook 'org-mode-hook 'linum-mode)
(add-hook 'org-mode-hook 'emojify-mode) ; This mode is :fire:!

;;; Babel with lot of active languages! :P
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (C          . t)
   ;; (asm        . t) ; ':) Comming soon (maybe...)
   (java       . t)
   (lua        . t)
   (ruby       . t)
   (shell      . t)
   (dot        . t)
   (haskell    . t)
   (js         . t)                     ; :thinking: JS, seriously?
   (scheme     . t)))

;;; Guix
(global-guix-prettify-mode)

;;; Scheme
(add-hook 'scheme-mode-hook 'guix-devel-mode)
(add-hook 'scheme-mode-hook 'paredit-mode)
(add-hook 'scheme-mode-hook 'linum-mode)
(add-hook 'scheme-mode-hook 'column-number-mode)
;; (add-hook 'scheme-mode-hook 'company-mode)

;;; Emacs lisp
(add-hook 'emacs-lisp-mode-hook 'paredit-mode)
(add-hook 'emacs-lisp-mode-hook 'linum-mode)
(add-hook 'emacs-lisp-mode-hook 'column-number-mode)
(add-hook 'emacs-lisp-mode-hook 'emojify-mode)
(if (display-graphic-p)
    (add-hook 'emacs-lisp-mode-hook 'pretty-mode))
;; (add-hook 'emacs-lisp-mode-hook 'company-mode)

;;; Ispell / Aspell
;; (ispell-change-dictionary "french" "globally")
(ispell-change-dictionary "francais" "globally")

;;; Flycheck grammalecte
(setq flycheck-grammalecte-report-apos nil)
(setq flycheck-grammalecte-enabled-modes
      '(org-mode        text-mode
        mail-mode       latex-mode
        markdown-mode   mu4e-compose-mode))

;;; LaTeX
;; (add-hook 'LaTex-mode-hook 'company-mode)
(add-hook 'LaTex-mode-hook 'flycheck-mode)
(add-hook 'org-mode-hook   'flycheck-mode)
(add-hook 'text-mode-hook  'flycheck-mode)


;;; Mode C
(setq-default c-default-style "gnu"     ; Use GNU indent style
              c-basic-offset 2          ; Use tow stace
              indent-tabs-mode nil)     ; Do not use tab

;;; Asm mode
(add-to-list 'auto-mode-alist '("\\.ys$" . asm-mode))
(add-to-list 'auto-mode-alist '("\\.S$" . asm-mode))

;;; OCAML
(add-to-list 'auto-mode-alist '("\\.ml$" . tuareg-mode))
;; (add-hook 'tuareg-mode-hook 'company-mode)


;;; ERC
(setq erc-nick point-emacs-user-pseudo)

;;; Spyglass
(autoload 'spyglass
  "~/.emacs.d/spyglass-mode/spyglass-mode" "" t nil)
(autoload 'spyglass-token-gen
  "~/.emacs.d/spyglass-mode/spyglass-mode" "" t nil)
(autoload 'spyglass-mode
  "~/.emacs.d/spyglass-mode/spyglass-mode" "" t nil)

(setq spyglass-server-token "<token>")

(setq spyglass-hosts-list
      '(("GNU web site (ping)"  "gnu.org"      "ping")
        ("Framagit (ping)"      "framagit.org" "ping")
        ("My server (spyglass)" "<server>"     "spyglass")))

(when (equal (system-name) "fry")
  (setq spyglass-hosts-list
        (cons '("My local server (spyglass)" "localhost:8080" "spyglass")
              spyglass-hosts-list)))

;;; PDF

(pdf-tools-install)

;; Set pdf color theme
(setq pdf-view-midnight-colors '("#d4d4d4" . "#000000"))

(add-hook 'pdf-view-mode-hook 'pdf-view-midnight-minor-mode)

;; ACSL

(defvar pretty-acsl-rules
  '(("\\\\forall" . ?∀)
    ("\\\\exists" . ?∃)
    ;; ("integer" . ?ℤ)
    ("<==>" . ?⟺)
    ("==>" . ?⟹)
    ("\|\|" . ?∨)
    ("&&" . ?∧)
    ("\\!=" . ?≠)
    ("<=" . ?≤)
    (">=" . ?≥)
    ("==" . ?≡)
    ("\\!" . ?¬)))

(defun pretty-acsl ()
  (mapc
   (lambda (x)
     (let ((old (car x))
           (new (cdr x)))
       (font-lock-add-keywords
        nil `((,(concatenate 'string
                           "\\(^\\|[^a-zA-Z0-9]\\)\\("
                           old "\\)[a-zA-Z=]")
             (0 (progn
                  (decompose-region (match-beginning 2) (match-end 2))
                  nil)))))
       (font-lock-add-keywords
        nil `((,(concatenate 'string "\\(^\\|[^a-zA-Z0-9]\\)\\(" old "\\)[^a-zA-Z=]")
             (0 (progn
                  (compose-region (match-beginning 2) (match-end 2)
                                  ,new)
                  nil)))))))
   pretty-acsl-rules))

(if (file-exists-p "~/.opam/default/share/emacs/site-lisp/acsl.el")
    (progn
      (load-file "~/.opam/default/share/emacs/site-lisp/acsl.el")
      (load-file "~/.opam/default/share/emacs/site-lisp/frama-c-init.el")))

(when (boundp 'acsl-keywords)
  (add-hook 'acsl-mode-hook 'pretty-acsl))

;;;
;;; Org-mode tempo
;;;

(setq rrmooc/new-org-templates (version<= "9.2" (org-version)))
(when  rrmooc/new-org-templates
  (require 'org-tempo))

(require 'subr-x)
(defun rrmooc/add-org-template (old-style-template)
  (add-to-list 'org-structure-template-alist
           (if rrmooc/new-org-templates
           (cons
            (first old-style-template)
            (string-trim-right (substring (second old-style-template) 8 -9)))
         old-style-template)))

(unless rrmooc/new-org-templates
  ;; this template is predefined in the new templating system
  (rrmooc/add-org-template
   '("s" "#+begin_src ?\n\n#+end_src" "<src lang=\"?\">\n\n</src>")))

(rrmooc/add-org-template
 '("B" "#+begin_src shell :session *shell* :results output :exports both \n\n#+end_src" "<src lang=\"sh\">\n\n</src>"))

;; (rrmooc/add-org-template
;;  '("LH" "#+LATEX_HEADER: " ""))

;; (rrmooc/add-org-template
;;  '("lh" "#+LATEX_HEADER: " ""))

(rrmooc/add-org-template
 '("t" "#+BEGIN_EXPORT latex
\\begin{table}
\\centering
\\resizebox{0.6\\linewidth}{!}{

}
%\\vspace{0.5pt}
%\\caption{Number of tests by errors types}
\\end{table}
#+END_EXPORT" ""))

;;;
;;; Function
;;;

(defun my-copyright ()
  "Insert copyright with name and email in current buffer."
  (interactive)
  (copyright (format "%s <%s>"
                     point-emacs-user-name
                     point-emacs-user-email)))

(defun this-is-life ()
  "Insert ¯\\_(ツ)_/¯ in current buffer."
  (interactive)
  (insert "¯\\_(ツ)_/¯"))

(defun insert-lambda ()
  "Insert λ in current buffer."
  (interactive)
  (insert "λ"))

(defun gpl-header ()
  (interactive)
  (let ((pos (point)))
    (goto-char (point-min))
    (let ((gpl (mapc
                (lambda (x)
                  (format "%s %s %s\n"
                          comment-start
                          x
                          comment-end))
                '("This program is free software: you can redistribute it and/or modify"
                  "it under the terms of the GNU General Public License as published by"
                  "the Free Software Foundation, either version 3 of the License, or"
                  "(at your option) any later version."
                  ""
                  "This program is distributed in the hope that it will be useful,"
                  "but WITHOUT ANY WARRANTY; without even the implied warranty of"
                  "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"
                  "GNU General Public License for more details."
                  ""
                  "You should have received a copy of the GNU General Public License"
                  "along with this program.  If not, see <https://www.gnu.org/licenses/>.")))))
    (insert
     (string-join gpl))))

;;; Todo list
(defun todo-list ()
  (interactive)
  (occur "TODO\\|FIXME\\|HACK"))

;;; Color hexa color
(defun hex-color ()
  (interactive)
  (font-lock-add-keywords
   nil
   '(("#[[:xdigit:]]\\{6\\}"
      (0 (put-text-property
          (match-beginning 0)
          (match-end 0)
          'face (list :background (match-string-no-properties 0)))))))
  (font-lock-flush))

(defun eval-string (str)
  "Eval STR as sexp in string format. Return t on success and nil
on error."
  (with-temp-buffer
    (insert str)
    (condition-case nil
        (progn (eval-buffer) t)
      (error (message "eval-string: error !") nil))))

;;;
;;; Key binding
;;;

(global-set-key (kbd "C-x t") 'todo-list)
(global-set-key (kbd "C-c l") 'insert-lambda)

;;; Magit
(global-set-key (kbd "C-x g") 'magit-status)

;;; Helm
(helm-mode t)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "C-x C-b") 'helm-buffers-list)
(global-set-key (kbd "M-x")     'helm-M-x)

(put 'downcase-region 'disabled nil)


;; (run-geiser 'guile)

(provide 'point_emacs)
;;; point_emacs.el ends here
